const express = require("express");
const path = require("path");
const app = express();
const { PORT = 3000 } = process.env;






//Static folder

app.use('/assets', express.static(path.join(__dirname, "public")));
//contacts API routes
app.use('/api/v1/contacts', require("./routes/api/v1/contacts") );

//Data -Parsing
app.use(express.json());
//Parse nested form-data.
app.use(express.urlencoded({ extended: false }));

app.listen(PORT, () => console.log(`Server start on port ${PORT}...`));
//Default Route for Website.
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});



//Menu route
app.get("/menu", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "menu.html"));
});

//About Route
app.get("/about", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "about.html"));
});

//Contact Route
app.get("/contact", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "contact.html"));
});



//for random requests
app.get("/*", (req, res) => {
  res.send("Page not found");
});


module.exports = app;